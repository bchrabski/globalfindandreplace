/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

var moduleRef;

var parameterName;

var runningProcess = false;

$(function () {
  if (window.RM) {
    var prefs = new gadgets.Prefs();
    parameterName = prefs.getString('parameterName');

    $('#selectTermsButton').on('click', function () {
      selectGlossaryTerms();
    });

    $('#findAllTerms').on('click', function () {
      findAllGlossaryTerms();
    });

    gadgets.window.adjustHeight();

    //
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $('.status')
      .addClass('incorrect')
      .html('<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.');
    gadgets.window.adjustHeight();
  }
});

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, async function (selected) {
  var configurationURL = '';
  var projectURL = '';
  var outcome = '';
  if (selected.length === 1) {
    unlockButton('#actionButton');
    $('#actionButton').unbind();
    $('.status').removeClass('warning correct incorrect');
    $('#actionButton')
      .css('display', 'inline')
      .on('click', async function () {
        //tutaj
        lockAllButtons();
        $('.status').removeClass('warning correct incorrect');
        $('.status').addClass('warning').html('<b>Message:</b> Comparing artefacts in progress.');

        var configuration = await getConfiguration();

        if (configuration.code === RM.OperationResult.OPERATION_OK) {
          var context = configuration.data;
          var componentURL = context.localComponentUri;
          configurationURL = context.localConfigurationUri;
          projectURL = getProjectAreaURL(componentURL);
          var serverURL = projectURL.split('/process/project-areas/')[0];
          var _xLnk_Pspec = getParametersURL('_xLnk_Pspec', componentURL, projectURL, configurationURL);
          var zAdm_LnkText_Differs = getParametersURL(
            'zAdm_LnkText_Differs',
            componentURL,
            projectURL,
            configurationURL
          );
          if (
            _xLnk_Pspec.parameterURI == '' ||
            _xLnk_Pspec.parameterURI == null ||
            _xLnk_Pspec.parameterURI == undefined
          ) {
            clearSelectedInfo();
            $('.status')
              .addClass('incorrect')
              .html(`<b>Error:</b> The attribute _xLnk_Pspec does not exist in the project area.`);
            lockAllButtons();
            gadgets.window.adjustHeight();
            runningProcess = false;
            return;
          }

          if (
            zAdm_LnkText_Differs.parameterURI == '' ||
            zAdm_LnkText_Differs.parameterURI == null ||
            zAdm_LnkText_Differs.parameterURI == undefined
          ) {
            clearSelectedInfo();
            $('.status')
              .addClass('incorrect')
              .html(`<b>Error:</b> The attribute zAdm_LnkText_Differs does not exist in the project area.`);

            gadgets.window.adjustHeight();
            runningProcess = false;
            return;
          }
        } else {
          clearSelectedInfo();
          $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
          lockAllButtons();
          gadgets.window.adjustHeight();
          runningProcess = false;
          return;
        }
        //HERE
        let commonTypeDiffer = new Set();
        let commonTypes = new Set();

        if (zAdm_LnkText_Differs.types.length > 0 && _xLnk_Pspec.types.length > 0) {
          for (item of zAdm_LnkText_Differs.types) {
            commonTypeDiffer.add(item);
          }
          for (item of _xLnk_Pspec.types) {
            if (commonTypeDiffer.has(item)) {
              commonTypes.add(item);
            }
          }
        }

        if (commonTypes.size == 0) {
          clearSelectedInfo();
          $('.status')
            .addClass('incorrect')
            .html(
              `<b>Error:</b> There are no artifact types that have both zAdm_LnkText_Differs and _xLnk_Pspec attributes.`
            );

          gadgets.window.adjustHeight();
          runningProcess = false;
          return;
        }

        //end

        process(selected, projectURL, configurationURL, commonTypes);
      });
    $('.status').removeClass('warning correct incorrect');
    $('.status').html('<b>Message:</b> One artifact is selected.');
  } else if (selected.length > 1) {
    unlockButton('#actionButton');
    $('#actionButton').unbind();
    $('.status').removeClass('warning correct incorrect');
    $('#actionButton')
      .css('display', 'inline')
      .on('click', async function () {
        lockAllButtons();
        $('.status').removeClass('warning correct incorrect');
        $('.status').addClass('warning').html('<b>Message:</b> Comparing artefacts in progress.');
        var configuration = await getConfiguration();

        if (configuration.code === RM.OperationResult.OPERATION_OK) {
          var context = configuration.data;
          var componentURL = context.localComponentUri;
          var configurationURL = context.localConfigurationUri;
          projectURL = getProjectAreaURL(componentURL);
          var serverURL = projectURL.split('/process/project-areas/')[0];
          var _xLnk_Pspec = getParametersURL('_xLnk_Pspec', componentURL, projectURL, configurationURL);
          var zAdm_LnkText_Differs = getParametersURL(
            'zAdm_LnkText_Differs',
            componentURL,
            projectURL,
            configurationURL
          );
          if (
            _xLnk_Pspec.parameterURI == '' ||
            _xLnk_Pspec.parameterURI == null ||
            _xLnk_Pspec.parameterURI == undefined
          ) {
            clearSelectedInfo();
            $('.status')
              .addClass('incorrect')
              .html(`<b>Error:</b> The attribute _xLnk_Pspec does not exist in the project area.`);

            gadgets.window.adjustHeight();
            runningProcess = false;
            return;
          }

          if (
            zAdm_LnkText_Differs.parameterURI == '' ||
            zAdm_LnkText_Differs.parameterURI == null ||
            zAdm_LnkText_Differs.parameterURI == undefined
          ) {
            clearSelectedInfo();
            $('.status')
              .addClass('incorrect')
              .html(`<b>Error:</b> The attribute zAdm_LnkText_Differs does not exist in the project area.`);
            lockAllButtons();
            gadgets.window.adjustHeight();
            runningProcess = false;
            return;
          }
        } else {
          clearSelectedInfo();
          $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
          lockAllButtons();
          gadgets.window.adjustHeight();
          runningProcess = false;
          return;
        }
        //HERE
        let commonTypeDiffer = new Set();
        let commonTypes = new Set();

        if (zAdm_LnkText_Differs.types.length > 0 && _xLnk_Pspec.types.length > 0) {
          for (item of zAdm_LnkText_Differs.types) {
            commonTypeDiffer.add(item);
          }
          for (item of _xLnk_Pspec.types) {
            if (commonTypeDiffer.has(item)) {
              commonTypes.add(item);
            }
          }
        }

        if (commonTypes.size == 0) {
          clearSelectedInfo();
          $('.status')
            .addClass('incorrect')
            .html(
              `<b>Error:</b> There are no artifact types that have both zAdm_LnkText_Differs and _xLnk_Pspec attributes.`
            );

          gadgets.window.adjustHeight();
          runningProcess = false;
          return;
        }

        //end
        process(selected, projectURL, configurationURL, commonTypes);
      });
    $('.status').html(`<b>Message:</b> ${selected.length} artifacts are selected.`);
  } else {
    // clear the display area...
    lockButton('#actionButton');
    $('#actionButton').unbind();
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $('.status').html('<b>Message:</b> Select one or multiple objects.');
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  $('#actionInModuleButton').attr('disabled', 'disabled');
  $('#actionInModuleButton').removeClass('btn-primary');
  $('#actionInModuleButton').addClass('btn-secondary');
  $('#actionInModuleButton').off('click');
  moduleRef = null;
});

async function process(selected, projectURL, configurationURL, commonTypes) {
  var respose;
  let compared = 0;
  let ignored = 0;

  return new Promise(async function (resolve, reject) {
    for (var q = 0; q < selected.length; q++) {
      var obj = selected[q];

      var opResult = await getAttributes(obj);

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $('.status').removeClass('warning correct incorrect');
        $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
      } else {
        lockAllButtons();
        $('.status').removeClass('warning correct incorrect');
        $('.status')
          .addClass('warning')
          .html(`<b>Message:</b> Processing artifact ${q + 1} out of ${selected.length}.`);

        var toSave = [];

        for (artAttrs of opResult.data) {
          var tmp = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
          var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE].name;
          var xLnk = artAttrs.values['_xLnk_Pspec'];
          var zAdm_LnkText_Differs = artAttrs.values['zAdm_LnkText_Differs'];

          if (artAttrs.ref.format.indexOf('#Text') > -1) {
            if (commonTypes.has(type)) {
              compared++;
              if (xLnk) {
                var links = xLnk.split(',');
                for (var lnk of links) {
                  var val = lnk.trim();
                  if (val) {
                    var value = parseInt(val);
                    var url = getArtefactURL(value, projectURL, configurationURL);
                    let targetPT = getPrimaryText(artAttrs.ref.uri, configurationURL);
                    if (url) {
                      let sourcePT = getPrimaryText(url, configurationURL);
                      if (targetPT == sourcePT) {
                        artAttrs.values['zAdm_LnkText_Differs'] = 'No';
                        toSave.push(artAttrs);
                      } else {
                        artAttrs.values['zAdm_LnkText_Differs'] = 'Yes';
                        toSave.push(artAttrs);
                        break;
                      }
                    } else {
                      artAttrs.values['zAdm_LnkText_Differs'] = 'Yes';
                      toSave.push(artAttrs);
                    }
                  }
                }
              } else {
                artAttrs.values['zAdm_LnkText_Differs'] = 'Yes';
                toSave.push(artAttrs);
              }
            } else {
              ignored++;
            }
          } else {
            ignored++;
          }

          //TODO: 2 Get Full IDs based on Web Ids
          //TODO: 3 Get full information about primary text
          //TODO: 4 Compare with primary text of orignial artefact
          //TODO: 5 Mark as dffierence if they are different

          //toSave.push(artAttrs);
        }
        if (toSave.length != 0) {
          var z = await modifyArtifacts(toSave);
          respose = z.code;
        } else {
          respose = RM.OperationResult.OPERATION_OK;
        }
      }

      if (respose != RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $('.status').removeClass('warning correct incorrect');
        $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
        return;
      }
    }
    unlockAllButtons();
    $('.status').removeClass('warning correct incorrect');
    $('.status')
      .addClass('correct')
      .html(
        `<b>Success:</b> All selected artifact(s) were processed. Number of artifacts compared: ${compared} and ignored: ${ignored}`
      );
    resolve('');
  });
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      unlockButton('#actionInModuleButton');
      $('#actionInModuleButton').on('click', processForModule);
    } else {
      lockButton('#actionInModuleButton');
    }
  }
}

async function processForModule() {
  var configurationURL = '';
  var projectURL = '';
  lockAllButtons();
  $('.status').removeClass('warning correct incorrect');
  $('.status').addClass('warning').html('<b>Message:</b> Comparing artefacts in progress.');

  var configuration = await getConfiguration();

  if (configuration.code === RM.OperationResult.OPERATION_OK) {
    var context = configuration.data;
    var componentURL = context.localComponentUri;
    configurationURL = context.localConfigurationUri;
    projectURL = getProjectAreaURL(componentURL);
    var serverURL = projectURL.split('/process/project-areas/')[0];
    var _xLnk_Pspec = getParametersURL('_xLnk_Pspec', componentURL, projectURL, configurationURL);
    var zAdm_LnkText_Differs = getParametersURL('zAdm_LnkText_Differs', componentURL, projectURL, configurationURL);
    if (_xLnk_Pspec.parameterURI == '' || _xLnk_Pspec.parameterURI == null || _xLnk_Pspec.parameterURI == undefined) {
      clearSelectedInfo();
      $('.status')
        .addClass('incorrect')
        .html(`<b>Error:</b> The attribute _xLnk_Pspec does not exist in the project area.`);
      lockAllButtons();
      gadgets.window.adjustHeight();
      runningProcess = false;
      return;
    }

    if (
      zAdm_LnkText_Differs.parameterURI == '' ||
      zAdm_LnkText_Differs.parameterURI == null ||
      zAdm_LnkText_Differs.parameterURI == undefined
    ) {
      clearSelectedInfo();
      $('.status')
        .addClass('incorrect')
        .html(`<b>Error:</b> The attribute zAdm_LnkText_Differs does not exist in the project area.`);
      lockAllButtons();
      gadgets.window.adjustHeight();
      runningProcess = false;
      return;
    }
  } else {
    clearSelectedInfo();
    $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
    lockAllButtons();
    gadgets.window.adjustHeight();
    runningProcess = false;
    return;
  }

  var result = await getModuleArtifacts(moduleRef);

  if (result.code !== RM.OperationResult.OPERATION_OK) {
    unlockButton('#actionInModuleButton');
    unlockButton('#selectTermsButton');
    $('.status').removeClass('warning correct incorrect');
    $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
  } else {
    var tmp = [];
    for (item of result.data) {
      tmp.push(item.ref);
    }
    //HERE
    let commonTypeDiffer = new Set();
    let commonTypes = new Set();

    if (zAdm_LnkText_Differs.types.length > 0 && _xLnk_Pspec.types.length > 0) {
      for (item of zAdm_LnkText_Differs.types) {
        commonTypeDiffer.add(item);
      }
      for (item of _xLnk_Pspec.types) {
        if (commonTypeDiffer.has(item)) {
          commonTypes.add(item);
        }
      }
    }

    if (commonTypes.size == 0) {
      clearSelectedInfo();
      $('.status')
        .addClass('incorrect')
        .html(
          `<b>Error:</b> There are no artifact types that have both zAdm_LnkText_Differs and _xLnk_Pspec attributes.`
        );

      gadgets.window.adjustHeight();
      runningProcess = false;
      return;
    }

    //end
    process(tmp, projectURL, configurationURL, commonTypes);
  }
}

function clearSelectedInfo() {
  $('.status').removeClass('incorrect correct warning').html('');

  $('.selected, .error').removeClass('selected error');
  $('.setRPNButton').css('display', 'none');
}

function getPrimaryText(url, configurationURL) {
  url = url.replace('resources/', 'publish/text?resourceURI=');
  url = url + `&oslc_config.context=${configurationURL}`;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open('GET', url, false);
  parameter_req.setRequestHeader('Accept', 'application/xml');
  parameter_req.send();

  var data;
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS(
      'http://jazz.net/xmlns/alm/rm/text/v0.1',
      'richTextBody'
    );

    if (objects.length < 1) {
      return '';
    } else {
      var tempText = objects[0].innerHTML;
      tempText = toPlainText(tempText);
      return tempText;
    }
  } catch (err) {
    return data;
  }
}

function getArtefactURL(artefactId, projectURL, configurationURL) {
  var serverURL = projectURL.split('/process/project-areas/')[0];

  var query =
    serverURL +
    '/views?oslc.query=true&projectURL=' +
    projectURL +
    '&oslc.prefix=dcterms=' +
    encodeURIComponent('<http://purl.org/dc/terms/>') +
    '&oslc.where=dcterms:identifier=' +
    artefactId +
    '&oslc.select=dcterms:title,dcterms:identifier';

  var parameter_req = new XMLHttpRequest();
  parameter_req.open('GET', query, false);
  parameter_req.setRequestHeader('Accept', 'application/rdf+xml');
  parameter_req.setRequestHeader('OSLC-Core-Version', '2.0');
  parameter_req.setRequestHeader('Configuration-Context', configurationURL);
  parameter_req.send();

  var data;
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS('http://open-services.net/ns/rm#', 'Requirement');

    for (i = 0; i < objects.length; i++) {
      var object = objects[i];
      data = object.getAttributeNS('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'about');
      return data;
    }
  } catch (err) {
    return data;
  }
}

function getParametersURL(parameterName, componentURL, projectURL, configurationURL) {
  var serverURL = projectURL.split('/process/project-areas/')[0];

  var query = serverURL + '/types?resourceContext=' + projectURL;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open('GET', query, false);
  parameter_req.setRequestHeader('Accept', 'application/xml');
  parameter_req.setRequestHeader('Content-Type', 'application/xml');
  parameter_req.setRequestHeader('OSLC-Core-Version', '2.0');
  parameter_req.setRequestHeader('Configuration-Context', configurationURL);
  parameter_req.send();
  var data = { parameterURI: '', types: [] };
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS(
      'http://www.ibm.com/xmlns/rdm/rdf/',
      'AttributeDefinition'
    );
    for (i = 0; i < objects.length; i++) {
      var object = objects[i];

      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == 'dcterms:title') {
          if (object.childNodes[c].innerHTML == parameterName) {
            data.parameterURI = object.getAttributeNS('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'about');
          }
        }
      }
    }

    if (data.parameterURI == '' || data.parameterURI == null || data.parameterURI == undefined) {
      return data;
    }

    objects = parameter_req.responseXML.getElementsByTagNameNS('http://www.ibm.com/xmlns/rdm/rdf/', 'ObjectType');
    for (i = 0; i < objects.length; i++) {
      var name = '';
      var type = '';
      var object = objects[i];
      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == 'dcterms:title') {
          name = object.childNodes[c].innerHTML;
        }

        if (object.childNodes[c].nodeName == 'rm:hasAttribute') {
          var hsAbout = object.childNodes[c].getAttributeNS('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'resource');
          if (hsAbout == data.parameterURI) {
            type = hsAbout;
          }
          var children = object.childNodes[c].childNodes;
          for (var q = 0; q < children.length; q++) {
            if (children[q].nodeName == 'rm:AttributeDefinition') {
              var adAbout = children[q].getAttributeNS('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'about');
              if (adAbout == data.parameterURI) {
                type = adAbout;
              }
            }
          }
        }
      }
      if (type != '') {
        data.types.push(name);
      }
    }

    return data;
  } catch (err) {
    clearSelectedInfo();
    $('.status')
      .addClass('incorrect')
      .html('<b>Error:</b> Something went wrong. Please try again, or contact your administrator.');
    lockAllButtons();
    gadgets.window.adjustHeight();
    return data;
  }
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open('GET', componentURL, false);
  projectArea_req.setRequestHeader('Accept', 'application/xml');
  projectArea_req.setRequestHeader('Content-Type', 'application/xml');

  try {
    projectArea_req.send();

    var project = projectArea_req.responseXML.getElementsByTagNameNS('http://jazz.net/ns/process#', 'projectArea');

    var projectURI = project[0].getAttributeNS('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'resource');
    return projectURI;
  } catch (err) {
    unlockAllButtons();
    $('.status').removeClass('warning correct incorrect');
    $('.status').addClass('warning').html(`<b>Error:</b> Parameters were not found`);
    $('.status').addClass('incorrect').html('<b>Error:</b> Something went wrong. Please try again.');
    return;
  }
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref,
      [
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.PRIMARY_TEXT,
        RM.Data.Attributes.IS_HEADING,
        RM.Data.Attributes.ARTIFACT_TYPE,
        '_xLnk_Pspec',
        'zAdm_LnkText_Differs',
      ],
      resolve
    );
  });
}

async function getConfiguration() {
  return new Promise(function (resolve, reject) {
    RM.Client.getCurrentConfigurationContext(resolve);
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function createTermLink(source, target) {
  return new Promise(function (resolve, reject) {
    RM.Data.createLink(source, RM.Data.LinkTypes.REFERENCES_TERM, target, resolve);
  });
}

function lockAllButtons() {
  $('#actionButton').attr('disabled', 'disabled');
  $('#actionButton').removeClass('btn-primary');
  $('#actionButton').addClass('btn-secondary');
  $('#selectTermsButton').attr('disabled', 'disabled');
  $('#selectTermsButton').removeClass('btn-primary');
  $('#selectTermsButton').addClass('btn-secondary');
  $('#findAllTerms').attr('disabled', 'disabled');
  $('#findAllTerms').removeClass('btn-primary');
  $('#findAllTerms').addClass('btn-secondary');
  $('#actionInModuleButton').attr('disabled', 'disabled');
  $('#actionInModuleButton').removeClass('btn-primary');
  $('#actionInModuleButton').addClass('btn-secondary');
}

function unlockAllButtons() {
  $('#actionButton').removeAttr('disabled');
  $('#actionButton').addClass('btn-primary');
  $('#actionButton').removeClass('btn-secondary');
  $('#selectTermsButton').removeAttr('disabled');
  $('#selectTermsButton').addClass('btn-primary');
  $('#selectTermsButton').removeClass('btn-secondary');
  $('#findAllTerms').removeAttr('disabled');
  $('#findAllTerms').addClass('btn-primary');
  $('#findAllTerms').removeClass('btn-secondary');

  if (moduleRef == null) return;
  $('#actionInModuleButton').removeAttr('disabled');
  $('#actionInModuleButton').addClass('btn-primary');
  $('#actionInModuleButton').removeClass('btn-secondary');
}

function lockButton(button) {
  $(button).attr('disabled', 'disabled');
  $(button).removeClass('btn-primary');
  $(button).addClass('btn-secondary');
}

function unlockButton(button) {
  if (button == '#actionInModuleButton' && moduleRef == null) return;
  $(button).removeAttr('disabled');
  $(button).addClass('btn-primary');
  $(button).removeClass('btn-secondary');
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(ref, [RM.Data.Attributes.PRIMARY_TEXT], resolve);
  });
}

function uriToPublishURI(uri) {
  var tmp = uri.replace('/resources/', '/publish/resources?resourceURI=');
  var url_req = new XMLHttpRequest();
  url_req.open('GET', tmp, false);
  url_req.setRequestHeader('Accept', 'application/xml');
  url_req.setRequestHeader('Content-Type', 'application/xml');

  try {
    url_req.send();
    var url = url_req.responseXML.getElementsByTagNameNS('http://www.ibm.com/xmlns/rrm/1.0/', 'core');
    var newURL = url[0].innerHTML;

    if (newURL) return uri.split('/resources/')[0] + '/resources/' + newURL;
    else return uri;
  } catch (err) {
    return;
  }
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

function toPlainText(text) {
  var textTmp;
  textTmp = text.replace(/<a(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<\/a(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<h(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<\/h(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<span(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<\/span(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<div(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<\/div(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<b>/gi, '');
  textTmp = textTmp.replace(/<\/b>/gi, '');
  textTmp = textTmp.replace(/<i>/gi, '');
  textTmp = textTmp.replace(/<\/i>/gi, '');
  textTmp = textTmp.replace(/<u>/gi, '');
  textTmp = textTmp.replace(/<\/u>/gi, '');
  textTmp = textTmp.replace(/style="[a-zA-Z0-9:;\.\s\(\)\-\,\#\:]*"/gi, '');
  textTmp = textTmp.replace(/<p(.|\n|\d)*?>/gi, '');
  textTmp = textTmp.replace(/<\/p(.|\n|\d)*?>/gi, '\n');
  textTmp = textTmp.replace(/^\s*\n/gm, '');
  textTmp = textTmp.replace(/<br>/gm, '');
  //textTmp=textTmp.replace(/&.+;/gm,'');
  return textTmp;
}

window.addEventListener('load', function () {
  gadgets.window.adjustHeight();
});

window.addEventListener('resize', function () {
  gadgets.window.adjustHeight();
});

$('.btn').on('click', function () {
  setTimeout(function () {
    gadgets.window.adjustHeight();
  }, 200);
});
