/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

var moduleRef;

var debugMode = false;

var results = [];

$(function () {
  if (window.RM) {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("Initialization");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Widget initialization Start");
    if (debugMode == "true") console.groupEnd("Initialization");
    // ***

    debugMode = prefs.getString("debugMode");

    // *** DEBUG MODE
    if (debugMode == "true") console.group("Initialization");
    if (debugMode == "true") console.log("Debug Mode ... Debug mode enabled: " + debugMode);

    if (debugMode == "true") console.groupEnd("Initialization");

    // ***

    // *** DEBUG MODE
    if (debugMode == "true") console.group("Initialization");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Widget initialization End");
    if (debugMode == "true") console.groupEnd("Initialization");
    // ***

    gadgets.window.adjustHeight();

    //
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, async function (selected) {
  if (selected.length === 1) {
    // *** DEBUG MODE ***
    if (debugMode == "true") console.group("Selection");
    if (debugMode == "true") console.log("Debug Mode ... Single artifact selected");
    if (debugMode == "true") console.groupEnd("Selection");
    // ***

    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Replacing value in progress.");
        $("#logs").attr("hidden", "hidden");
        gadgets.window.adjustHeight();
        process(selected);
      });
    $(".status").removeClass("warning correct incorrect");
    $(".status").html("<b>Message:</b> One artifact is selected.");
  } else if (selected.length > 1) {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("Selection");
    if (debugMode == "true") console.log("Debug Mode ... Multiple artifacts selected");
    if (debugMode == "true") console.groupEnd("Selection");
    //****

    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");
        $("#logs").attr("hidden", "hidden");
        gadgets.window.adjustHeight();
        process(selected);
      });
    $(".status").html(`<b>Message:</b> ${selected.length} artifacts are selected.`);
  } else {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("Selection");
    if (debugMode == "true") console.log("Debug Mode ... Zero artifacts selected");
    if (debugMode == "true") console.groupEnd("Selection");
    //**
    // clear the display area...
    lockButton("#actionButton");
    $("#actionButton").unbind();
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html("<b>Message:</b> Select one or multiple artifacts.");
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  $("#actionInModuleButton").off("click");

  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
  moduleRef = null;
});

async function process(selected) {
  results = [];

  const srcValue = $("#srcValue").val();
  const tgtValue = $("#tgtValue").val();

  if (!srcValue) {
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Error:</b> Value of <b>Find What</b> can not be empty.");
    return;
  }

  var outcome = "";
  var respose;

  return new Promise(async function (resolve, reject) {
    for (var q = 0; q < selected.length; q++) {
      var obj = selected[q];
      var uriTmp = uriToPublishURI(obj.uri);
      if (uriTmp) {
        let tmp2 = new RM.ArtifactRef(uriTmp, obj.componentUri, null, obj.format);
        obj = tmp2;
      }

      var opResult = await getAttributes(obj);

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        $("#logs").attr("hidden", "hidden");
        gadgets.window.adjustHeight();
      } else {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status")
          .addClass("warning")
          .html(`<b>Message:</b> Processing artifact ${q + 1} out of ${selected.length}.`);
        var toSave = [];

        for (artAttrs of opResult.data) {
          // Reset some attribute values

          // DODWANIE

          var primaryText = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
          var id = artAttrs.values[RM.Data.Attributes.IDENTIFIER];
          var name = artAttrs.values[RM.Data.Attributes.NAME];

          // DODWANIE

          var srcValueEscaped = escapeRegExp(srcValue);
          var tgtValueEscaped = escapeRegExp(tgtValue);
          var str = `(?!([^<]+)?>)${srcValueEscaped}`;
          var re = new RegExp(str, "g");

          // Primary Text
          var primaryTextModified = primaryText.replace(re, tgtValueEscaped);
          artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT] = primaryTextModified;
          if (primaryText === primaryTextModified) {
            primaryText = "Not changed";
          } else {
            primaryText = primaryTextModified;
          }

          // Name
          // var nameModified = name.replace(re, tgtValueEscaped);
          // artAttrs.values[RM.Data.Attributes.NAME] = nameModified;
          // artAttrs.values['Title'] = 'test';
          // console.log('Chuja');
          // console.log(artAttrs);
          // if (name === nameModified) {
          //   name = 'Not changed';
          // } else {
          //   name = nameModified;
          // }

          var obj = { id: id, name: name, primaryText: primaryText };
          results.push(obj);
          toSave.push(artAttrs);
        }

        var z = await modifyArtifacts(toSave);
        respose = z.code;
      }

      if (respose != RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        $("#logs").attr("hidden", "hidden");
        gadgets.window.adjustHeight();
      } else {
      }
    }

    if (outcome.length == 0) {
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("correct").html("<b>Success:</b> Value replaced in selected artifacts(s).");
      $("#logs").removeAttr("hidden");
      outcome += "Error";
      unlockAllButtons();
      gadgets.window.adjustHeight();
    } else {
      {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        $("#logs").attr("hidden", "hidden");
        gadgets.window.adjustHeight();
      }
    }
    resolve("");
  });
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];
    if (
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE ||
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.COLLECTION
    ) {
      if (moduleRef != attrs.ref) {
        moduleRef = attrs.ref;

        $("#actionInModuleButton").on("click", processForModule);

        unlockButton("#actionInModuleButton");
      }
    }
  }
}

async function processForModule() {
  lockAllButtons();
  $("#logs").attr("hidden", "hidden");
  gadgets.window.adjustHeight();
  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning").html("<b>Message:</b> Replacing value in progress.");

  var result = await getModuleArtifacts(moduleRef);

  if (result.code !== RM.OperationResult.OPERATION_OK) {
    unlockButton("#actionInModuleButton");
    unlockButton("#selectTermsButton");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
  } else {
    var tmp = [];
    for (item of result.data) {
      tmp.push(item.ref);
    }
    process(tmp);
  }
}

function clearSelectedInfo() {
  // *** DEBUG MODE
  if (debugMode == "true") console.group("removeAllTermsFromModule");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Clear selected info Start");
  if (debugMode == "true") console.groupEnd("removeAllTermsFromModule");
  // ***

  $(".status").removeClass("incorrect correct warning").html("");

  $(".selected, .error").removeClass("selected error");
  $(".setRPNButton").css("display", "none");

  // *** DEBUG MODE
  if (debugMode == "true") console.group("removeAllTermsFromModule");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Clear selected info End");
  if (debugMode == "true") console.groupEnd("removeAllTermsFromModule");
  // ***
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref,
      [
        RM.Data.Attributes.IDENTIFIER,
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.PRIMARY_TEXT,
        RM.Data.Attributes.IS_HEADING,
        RM.Data.Attributes.ARTIFACT_TYPE,
      ],
      resolve
    );
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

function lockAllButtons() {
  $("#actionButton").attr("disabled", "disabled");
  $("#actionButton").removeClass("btn-primary");
  $("#actionButton").addClass("btn-secondary");

  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  $("#actionButton").removeAttr("disabled");
  $("#actionButton").addClass("btn-primary");
  $("#actionButton").removeClass("btn-secondary");

  if (moduleRef == null) return;
  $("#actionInModuleButton").removeAttr("disabled");
  $("#actionInModuleButton").addClass("btn-primary");
  $("#actionInModuleButton").removeClass("btn-secondary");
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  if (button == "#actionInModuleButton" && moduleRef == null) return;
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(ref, [RM.Data.Attributes.PRIMARY_TEXT], resolve);
  });
}

function uriToPublishURI(uri) {
  var tmp = uri.replace("/resources/", "/publish/resources?resourceURI=");
  var url_req = new XMLHttpRequest();
  url_req.open("GET", tmp, false);
  url_req.setRequestHeader("Accept", "application/xml");
  url_req.setRequestHeader("Content-Type", "application/xml");

  try {
    url_req.send();
    var url = url_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rrm/1.0/", "core");
    var newURL = url[0].innerHTML;

    if (newURL) return uri.split("/resources/")[0] + "/resources/" + newURL;
    else return uri;
  } catch (err) {
    return;
  }
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

window.addEventListener("load", function () {
  gadgets.window.adjustHeight();
});

window.addEventListener("resize", function () {
  gadgets.window.adjustHeight();
});

$("#generateExcelReport").on("click", function () {
  $("#generateReport").attr("disabled", "disabled");
  var wb = XLSX.utils.book_new();
  wb.Props = {
    Title: "Find and Replace Results",
    Subject: "Find and Replace Results",
    Author: "Find and Replace Results",
  };

  wb.SheetNames.push("Find and Replace Results");
  // var wscols = [{ wch: 10 }, { wch: 15 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }];

  // var ws_data = [
  //   ["ID", "Name", "Primary Text", "__Chng_Proposal", "__Chng_Rationale", "seNotes_History", "_test_PassCriteria"],
  // ];

  var wscols = [{ wch: 10 }, { wch: 15 }, { wch: 40 }];

  var ws_data = [["ID", "Name", "Primary Text"]];

  for (var i = 0; i < results.length; i++) {
    var id = results[i].id;
    var name = results[i].name;
    var primaryText = results[i].primaryText;

    ws_data.push([id, name, primaryText]);
  }

  var ws = XLSX.utils.aoa_to_sheet(ws_data);
  ws["!cols"] = wscols;
  wb.Sheets["Find and Replace Results"] = ws;

  var wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });

  saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), "FindAndReplace-Results.xlsx");

  $("#generateExcelReport").removeAttr("disabled");
});

function s2ab(s) {
  var buf = new ArrayBuffer(s.length);
  var view = new Uint8Array(buf);
  for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff;
  return buf;
}

$(".btn").on("click", function () {
  setTimeout(function () {
    gadgets.window.adjustHeight();
  }, 200);
});
